#!/usr/bin/env python

import os

import numpy as np
import tensorflow as tf
from tensorflow.keras import layers as tfl


EXAMPLE_FILE = os.path.join(os.path.dirname(__file__), "example.flac")
CHECKPOINT = os.path.join(os.path.dirname(__file__), "analyzer-1")


def create_analyzer(padding="valid"):
    """Create the model for pulse analysis

    The original model was trained with padding="valid"
    however for inference padding="same" may be used
    in order to keep temporal alignment.
    """
    model = tf.keras.Sequential([
        tfl.Conv2D(512, (32, 1), padding=padding, name="conv1"),
        tfl.Activation("relu"),
        tfl.MaxPool2D((2, 1)),
        tfl.BatchNormalization(name="conv1-BN"),

        tfl.Conv2D(64, (32, 1), padding=padding, name="conv2"),
        tfl.Activation("relu"),
        tfl.MaxPool2D((2, 1)),
        tfl.BatchNormalization(name="conv2-BN"),

        tfl.Conv2D(64, (32, 1), padding=padding, name="conv3"),
        tfl.Activation("relu"),
        tfl.MaxPool2D((2, 1)),
        tfl.BatchNormalization(name="conv3-BN"),

        tfl.Conv2D(256, (32, 1), padding=padding, name="conv4"),
        tfl.Activation("relu"),
        tfl.BatchNormalization(name="conv4-BN"),

        tfl.Conv2D(512, (32, 1), padding=padding, name="conv5"),
        tfl.Activation("relu"),
        tfl.BatchNormalization(name="conv5-BN"),

        tfl.Conv2D(1024, (32, 1), padding=padding, name="conv6"),
        tfl.Activation("relu"),
        tfl.BatchNormalization(name="conv6-BN"),

        tfl.Conv2D(1, (4, 1), padding=padding, name="classifier",
                   activation="sigmoid"),
    ])
    model.build([None, None, 1, 1])
    return model


def restore_analyzer(padding="same", checkpoint_location=CHECKPOINT):
    """Create the analyzer model and restore its weights

    :param padding: will be passed to the function `create_analyzer`
    :param checkpoint_location: Location of the checkpoint. The default
        should work fine, but in case the checkpoint cannot be found
        the location can be overridden here.
    """
    model = create_analyzer(padding=padding)
    checkpoint = tf.train.Checkpoint(analyzer=model)
    checkpoint.restore(checkpoint_location)
    return model


def generate_pulse_signal(analyzer: tf.keras.Model, audio):
    """Generate the pulse signal from audio

    :param analyzer: Analysis module generated from `restore_analyzer`
    :param audio: Audio signal, expected to be a 1d numpy array
        with a sample rate of 16kHz
    :returns: Pulse signal as a 1d numpy array with sample rate of 2kHz
    """
    audio4d = audio[np.newaxis, :, np.newaxis, np.newaxis]
    pulses4d = analyzer(audio4d)
    return pulses4d[0, :, 0, 0].numpy()


def test_model(audio_file=EXAMPLE_FILE):
    """Convenience function to test if the model works"""
    from pysndfile import sndio
    audio, fs, _ = sndio.read(audio_file)
    if fs != 16000:
        print(f"Warning: sound file has sample rate {fs} Hz but the model "
              f"was trained with 16000 Hz audio. The analysis may not work "
              f"as expected.")
    audio4d = audio[np.newaxis, :, np.newaxis, np.newaxis]
    analyzer = restore_analyzer("same")
    pulses = analyzer(audio4d)
    pulses1d = pulses[0, :, 0, 0].numpy()
    return audio, pulses1d


def test_implementation(audio_file=EXAMPLE_FILE):
    """Convenience function to test if the implementation works"""
    from matplotlib import pyplot as plt
    audio, pulses = test_model(audio_file)
    t_audio = np.arange(audio.shape[0]) / 16000.
    plt.plot(t_audio, audio, label="Audio")
    t_pulse = np.arange(pulses.shape[0]) / 2000.
    plt.plot(t_pulse, pulses, label="Pulses")
    plt.legend()
    plt.show()


if __name__ == '__main__':
    import sys
    test_implementation(*sys.argv[1:])
