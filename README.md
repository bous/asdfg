# Semi-supervised learning of glottal pulse positions in a neural analysis-synthesis framework

This repository provides the pulse analysis module `Agamemnon-A`
from the paper

[Bous, Frederik, Luc Ardaillon, and Axel Roebel. "Semi-supervised learning of glottal pulse positions in a neural analysis-synthesis framework." 2020 28th European Signal Processing Conference (EUSIPCO). IEEE, 2020.](https://www.eurasip.org/Proceedings/Eusipco/Eusipco2020/pdfs/0000401.pdf)

If you use this implementation in your work
or find it useful for your research
please cite the above paper
in your publications.

## Usage

### Requirements

This implementation was tested with tensorflow-gpu2.3
but it is likely to work with older versions of tensorflow 2.

### Basic example

To see if everything works fine, just run

```bash
./model.py
```

or

```bash
./model.py <some_audio_file>
```

which will create the model,
load the weights,
analyse the audio file
and create a plot
with the input audio file and the generated pulse sequence.
The audio file should have a sample rate of 16 kHz.

*Note: for this example you need the package
[pysndfile](https://pypi.org/project/pysndfile/),
to load the audio file
which otherwise is not a dependency.*

### Library

For real usage of the analyser
use the following functions
from the module `model.py`:
<table>
  <th>Function</th><th>Description</th>
  <tr>
    <td><code>create_analyzer</code></td>
    <td>Create a model with random weights.</td>
  </tr>
  <tr>
    <td><code>restore_analyzer</code></td>
    <td>Create the model and load the pretrained weights.</td>
  </tr>
  </tr>
  <tr>
    <td><code>generate_pulse_signal</code></td>
    <td>
      Apply the analyzer to the given audio file.
    </td>
  </tr>
</table>

For more details please refer to the documentation
in the [source code](model.py)

### Remarks

* The model was trained on 16kHz audio
  and will likely only work with inputs at 16kHz.
  The output is at 2kHz
  (or 1/8th of the input sampling frequency)
  due to pooling operations.
* The model that is created with `restore_analyzer`
  expects a 4d tensor with the dimensions
  - (#batch_samples, #time_samples, 1, 1)

  The number of time samples can be anything
  as long as it is more than 993.
  During inference the number of batch samples
  is most likely 1,
  but multiple audio files may be analysed in parallel
  (provided their number of time samples is the same).
* The model was trained with `"valid"` padding,
  but can be used with `"same"` padding during inference
  which simplifies temporal alignment.
  The padding method can be controlled with the
  `padding` parameter in the functions
  `restore_analyzer` and `create_analyzer`


## Attribution
The provided example audio file
is from the [ARU dataset](http://datacat.liverpool.ac.uk/681/)
and provided under the *Creative Commons: Attribution 3.0* license.
The file was resampled to 16 kHz.
